package  
{
	import flash.net.Socket;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class NetworkSend 
	{
		
		public function NetworkSend() 
		{
			
		}
		
		public static function Authentification(socket:Socket):void
		{
			Debug.GetInstance().println("<= Auth");
			var packet:SfmlPacket = new SfmlPacket();
			packet.addUInt16(Opcode.Get(Opcode.CLIENT_TRACKER, Opcode.CONN_MASTER));
			packet.addString("test");
			packet.addString("0fd7bf4aef2dbe4e71ac2cc93e75ed02d9931822");
			packet.addString("127.0.0.19");
			packet.addInt16(0);
			packet.send(socket);
		}
				
		public static function GetFilm(socket:Socket, id:int):void
		{
			Debug.GetInstance().println("<= GetFilm " + id);
			var packet:SfmlPacket = new SfmlPacket();
			packet.addUInt16(Opcode.Get(Opcode.CLIENT_TRACKER, Opcode.CT_URL));
			packet.addInt(id);
			packet.send(socket);
		}
	}

}