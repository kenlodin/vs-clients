package  
{
	import mx.controls.VideoDisplay;
	import spark.components.VideoPlayer;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Player 
	{
		private var player:VideoPlayer;
		
		private static var _instance:Player;
		
		public static function GetInstance() : Player
		{
			if (_instance == null) _instance = new Player();
			return _instance;
		}
		
		public function Player() 
		{
			
		}
		
		public function setPlayer(player:VideoPlayer):void
		{
			this.player = player;
		}
		
		public function Play(source:String):void
		{
			player.source = source;
			player.autoPlay = true;
			//player.play();
		}
	}

}