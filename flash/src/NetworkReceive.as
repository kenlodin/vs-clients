package  
{
	import Action.Action;
	import flash.net.Socket;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class NetworkReceive 
	{
		private static var _instance:NetworkReceive;
		
		private var size : int = -1;
		
		private var action : Action = null;
		
		public static function GetInstance() : NetworkReceive
		{
			if (_instance == null) _instance = new NetworkReceive();
			return _instance;
		}
		
		public function NetworkReceive() 
		{
			
		}
		
		public function Reveive(socket:Socket) : void
		{
			var opcode : int;
			
			if (socket.bytesAvailable == 0)
				return;
			if (action != null)
			{
				if (action.Receive(socket))
				{
					action = null;
					size = -1;
					Debug.GetInstance().println("Finish action");
				}
				return;
			}
			
			if (size == -1)
				size = socket.readInt();
			if (socket.bytesAvailable == 0)
				return;
			
			opcode = socket.readUnsignedShort();
			if (Opcode.GetAction(opcode) == Opcode.TOKEN)
				action = new Action.Auth(size);
			else if (Opcode.GetAction(opcode) == Opcode.TC_URL)
				action = new Action.Url(size);
			else
				Debug.GetInstance().println("Error opcode:" + opcode);
			if (action.Receive(socket))
			{
				action = null;
				size = -1;
				Debug.GetInstance().println("Finish action2");
			}
			
		}
	}
}