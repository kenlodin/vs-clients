package Action 
{
	import flash.net.Socket;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Auth implements Action
	{
		
		private var size : int;
		private var str_size : int;
		
		public function Auth(size:int) 
		{
			this.size = size;
		}
		
		public function Receive(socket:Socket) : Boolean
		{
			if (socket.bytesAvailable == 0)
			  return false;
			str_size = socket.readInt();
			var b : ByteArray = new ByteArray();
			socket.readBytes(b, 0, str_size);
			Debug.GetInstance().println("=> Auth[" + b.toString() + "]");
			return true;
		}
		
	}

}