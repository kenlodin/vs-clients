package Action 
{
	import flash.net.Socket;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Url implements Action
	{
		
		private var size : int;
		private var str_size : int;
		private var ip : String = null;
		private var link : String;
		
		public function Url(size:int) 
		{
			this.size = size;
		}
		
		public function Receive(socket:Socket) : Boolean
		{
			var b : ByteArray = new ByteArray();
			if (socket.bytesAvailable == 0)
			  return false;
			str_size = socket.readInt();
			if (ip == null)
			{
				socket.readBytes(b, 0, str_size);
				ip = b.toString();
				if (socket.bytesAvailable == 0)
					return false;
			}
			str_size = socket.readInt();
			socket.readBytes(b, 0, str_size);
			link = b.toString();
			Debug.GetInstance().println("=>Getfilm ip:" + ip + " link:" + link);
			Debug.GetInstance().println("Video at http://" + ip + ":8000" + link);
			Player.GetInstance().Play("http://" + ip + ":8000" + link);
			return true;
		}
		
	}

}