package Action 
{
	import flash.net.Socket;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public interface Action 
	{
		function Receive(socket:Socket) : Boolean;
	}

}