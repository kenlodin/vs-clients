package  
{
	import flash.net.Socket;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class SfmlPacket 
	{
		
		/* To send */
		
	    private var b:ByteArray = new ByteArray();
		private var socket:Socket;
		private var pos:int = 0;
		
		public function SfmlPacket() 
		{
		}
		
		public function addInt16(i:int):void
		{
			b.writeShort(i);
		}
				
		public function addUInt16(i:int):void
		{
			b.writeShort(i);
		}
		
		public function addInt(i:int):void
		{
			b.writeInt(i);
		}
		
		public function addString(s:String):void
		{
			b.writeInt(s.length);
			for (var i:Number = 0; i < s.length; i++)
			{
				b.writeByte(s.charCodeAt(i));
			}
		}
		
		public function send(socket:Socket):void
		{
			socket.writeInt(b.length);
			socket.writeBytes(b);
			socket.flush();
		}
		
		
		/* To receive */
		
		public function setSocket(sock:Socket) : void
		{
			this.socket = sock;
		}
		
		public function getUInt16() : int
		{
			var res:int;
			res = socket.readUnsignedShort();
			pos += 2;
			return res;
		}
		
		public function getInt32() : int
		{
			var res:int = socket.readInt();
			pos += 4;
			return res;
		}
		
		public function getString() : String
		{
			//int size = this.getInt32();
			return socket.readUTF();
		}
	}

}