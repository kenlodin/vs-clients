package  
{
	import flash.display.MovieClip;
	import flash.system.Security;
	import flash.net.Socket;
	import flash.events.*;
	import mx.controls.TextArea;
	import org.osmf.elements.VideoElement;
	import org.osmf.media.MediaPlayerSprite;
	import org.osmf.media.URLResource;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Test extends MovieClip
	{
		private var textArea:TextArea;
		private var socket:Socket;
		private var host:String = "37.59.85.217";
		private var post:int = 36000;
		private var film_id:int = 0;
		
		
		public function Test() 
		{
		
		}
		
		public function launch(id:int) : void
		{
			film_id = id;
			//Security.allowDomain(host);
		    //Security.allowInsecureDomain(host);
			socket = new Socket(host, post);
			socket.addEventListener(Event.CONNECT, socketConnect);
			socket.addEventListener(IOErrorEvent.IO_ERROR, socketError);
			socket.addEventListener(Event.CLOSE, socketClose);		
			socket.addEventListener(ProgressEvent.SOCKET_DATA, socketData);
			socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityError);
			
			try {
                //this.socket.connect(host, post);
            }
            catch (e:Error) {
                Debug.GetInstance().println("Error on connect: " + e);
            }
			Debug.GetInstance().println("End of the method");
			//socket.close();
		}
		
		public function setTextArea(t:TextArea):void
		{
			textArea = t;
		}
		
		private function socketConnect(event:Event):void {
            //Debug.GetInstance().println("Connected: " + event);
			NetworkSend.Authentification(socket);
			NetworkSend.GetFilm(socket, film_id);
        }
		
		private function socketError(event:IOErrorEvent):void {
            //Debug.GetInstance().println("Socket error: " + event);
        }

		
		private function socketData(event:ProgressEvent):void {
            Debug.GetInstance().println("Receiving data: " + event);
			NetworkReceive.GetInstance().Reveive(socket);
        }
		
		private function socketClose(event:Event):void {
            Debug.GetInstance().println("Connection closed: " + event);
        }
		
		private function securityError(event:SecurityErrorEvent):void {
            Debug.GetInstance().println("Security error: " + event);
            //Debug.GetInstance().println("Security error: " + event.text);
		}

	}

}