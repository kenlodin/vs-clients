package  
{
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Opcode 
	{
		public static function Get(type:int, code:int):int {
			return (type << 12) + code;
		}
		
		public static function GetType(opcode:int):int {
			return opcode >> 12;
		}
    
		public static function GetAction(opcode:int):int {
			return opcode & 0xFFF;
		}
		
        public static var CLIENT_TRACKER:int = 0;
        public static var TRACKER_CLIENT:int = 1;
        public static var CLIENT_DIFF:int = 2;
        public static var DIFF_CLIENT:int = 3;
		
		
        public static var CONN_MASTER:int = 0;
        public static var CONN_SLAVE:int = 1;
        public static var ASK_LIST:int = 2;
        public static var ASK_FLUX:int = 3;
        public static var ASK_CHECK:int = 4;
        public static var ASK_PACKET:int = 5;
        public static var ASK_RPACKET:int = 6;
        public static var ASK_MOVE:int = 7;
        public static var ASK_DEFICIENT:int = 8;
        public static var ASK_REM:int = 9;
        public static var ASK_STOP:int = 10;
        public static var DEC:int = 11;
        public static var CT_PING:int = 12;
        public static var CT_URL:int = 13;
		
        public static var TOKEN:int = 0;
        public static var LIST:int = 1;
        public static var LIST_DIFF:int = 2;
        public static var LIST_NDIFF:int = 3;
        public static var MSG:int = 4;
        public static var TC_PING:int = 5;
        public static var TC_URL:int = 6;
		
        public static var SND_TOKEN:int = 0;
		
        public static var DATA:int = 0;
	}

}