package  
{
	import mx.controls.TextArea;
	/**
	 * ...
	 * @author Corentin Derbois
	 */
	public class Debug 
	{
		
		private var ui:TextArea = null;
		private static var _instance:Debug;
		
		public static function GetInstance() : Debug
		{
			if (_instance == null) _instance = new Debug();
			return _instance;
		}
		
		public function SetDebug(pui:TextArea) : void
		{
			ui = pui;
			ui.text = "=== Bienvenue dans ce debug :) ===";
		}
		
		public function println(str:String) : void
		{
			if (ui != null)
				ui.text = ui.text + '\n' + str;
		}
		
	}

}