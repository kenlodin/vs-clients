#include "avireader.hpp"


AviReader::AviReader(std::string str, e_mode mode)
{
  if (mode == MODE_READ)
  {
    fd = open(str.c_str(), O_RDONLY);
    if (fd == -1)
      exit(1);
  }
  else
  {
    fd = open(str.c_str(), O_CREAT | O_RDWR, 0666);
  }
}

void
AviReader::closed()
{
  close(fd);
}

void
AviReader::putChunk(sf::Packet &packet, int with_data)
{
  s_chunk *chunk;

  chunk = getChunk(with_data);
  packet.Append(chunk, 12);
  if (with_data)
  {
    packet.Append(chunk->data, chunk->size - sizeof(u32));
    free(chunk->data);
  }
  free(chunk);
}
    
void
AviReader::takeChunk(sf::Packet &packet, int with_data)
{
  s_chunk chunk;

  std::memcpy(&chunk, packet.GetData() + 1, 12);
  printf("type:%.4s name:%.4s size:%u\n", chunk.fcc, chunk.name, chunk.size);
  chunk.data = NULL;
  if (with_data)
  {
    printf("With data\n");
    chunk.data = malloc(chunk.size - 4);
    std::memcpy(chunk.data, packet.GetData() + 12 + 1, chunk.size - 4); 
  }
  setChunk(&chunk);
}

void 
AviReader::putSubChunk(sf::Packet &packet)
{
  s_sub_chunk *sub_chunk;

  sub_chunk = getSubChunk();
  packet.Append(sub_chunk, 8);
  packet.Append(sub_chunk->data, sub_chunk->size);
}
    
void
AviReader::takeSubChunk(sf::Packet &packet)
{
  s_sub_chunk sub_chunk;

  std::memcpy(&sub_chunk, packet.GetData() + 1, 8);
  printf("name:%.4s size:%d\n", sub_chunk.fcc, sub_chunk.size);
  sub_chunk.data = malloc(MOD2(sub_chunk.size));
  std::memcpy(sub_chunk.data, packet.GetData() + 8 + 1, MOD2(sub_chunk.size));
  setSubChunk(&sub_chunk);
}

s_chunk*
AviReader::getChunk(int with_data)
{
  s_chunk* chunk;

  chunk = reinterpret_cast<s_chunk*>(malloc(sizeof(s_chunk)));
  read(fd, chunk, SIZE_CHUNK_HEADER);
  printf("type:%.4s name:%.4s size:%u\n", chunk->fcc, chunk->name, chunk->size);
  chunk->data = NULL;

  // If we want take header and data
  if (with_data)
  {
    chunk->data = malloc(chunk->size - sizeof(u32));
    read(fd, chunk->data, chunk->size - sizeof(u32));
  }

  return chunk;
}

void
AviReader::setChunk(s_chunk *chunk)
{
  write(fd, chunk, 12);
  if (chunk->data)
    write(fd, chunk->data, chunk->size - 4);
}

s_sub_chunk*
AviReader::getSubChunk()
{
  s_sub_chunk *sub_chunk;

  sub_chunk = reinterpret_cast<s_sub_chunk*>(malloc(sizeof(s_sub_chunk)));
  read(fd, sub_chunk, SIZE_SUBCHUNK_HEADER);
  printf("type:%.4s\n", sub_chunk->fcc);
  sub_chunk->data = malloc(MOD2(sub_chunk->size));
  read(fd, sub_chunk->data, MOD2(sub_chunk->size));

  return sub_chunk;
}

void
AviReader::setSubChunk(s_sub_chunk *sub_chunk)
{
  write(fd, sub_chunk, 8);
  write(fd, sub_chunk->data, MOD2(sub_chunk->size));
}
