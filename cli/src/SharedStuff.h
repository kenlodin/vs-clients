/* 
 * File:   SharedStuff.h
 * Author: kokarez
 *
 * Created on 23 avril 2012, 22:00
 */

#ifndef SHAREDSTUFF_H
# define	SHAREDSTUFF_H

# include <boost/thread/mutex.hpp>

class SharedStuff {
public:
    SharedStuff();
    static SharedStuff *getInstance();
private:
    static SharedStuff *instance_;
    boost::mutex mutexOfGod_;
};

#endif	/* SHAREDSTUFF_H */

