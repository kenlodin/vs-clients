#ifndef AVIREADER__HH__
# define AVIREADER__HH__

# include <string>
# include <SFML/Network.hpp>
# include <stdio.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <cstring>



# define SIZE_CHUNK_HEADER (sizeof(s_chunk) - sizeof(u32))
# define SIZE_SUBCHUNK_HEADER (sizeof(s_sub_chunk) - sizeof(u32))
# define MOD2(X) ((X) + (X % 2))

typedef unsigned long long u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;

typedef enum
{
  MODE_READ,
  MODE_WRITE,
} e_mode;

typedef struct
{
  char      fcc[4];
  u32  size;
  void      *data;
} s_sub_chunk;

typedef struct
{
  char      fcc[4];
  u32  size;
  char      name[4];
  void      *data;
} s_chunk;

class AviReader
{
  public:
    AviReader() {};
    AviReader(std::string str, e_mode mode);
    void closed();

    void putChunk(sf::Packet &packet, int with_data);
    void takeChunk(sf::Packet &packet, int with_data);

    void putSubChunk(sf::Packet &packet);
    void takeSubChunk(sf::Packet &packet);

  private:
    s_chunk *getChunk(int with_data);
    void setChunk(s_chunk *chunk);

    s_sub_chunk *getSubChunk();
    void setSubChunk(s_sub_chunk *sub_chunk);

  private:
    int fd;
};

#endif /* !AVIREADER__HH__ */
