#ifndef CLIENT__HH__
# define CLIENT__HH__

# include <SFML/Network.hpp>
# include <string>
# include <iostream>

# include "sha1.h"
# include "opcode.hpp"

class Client
{
  public:
    Client(std::string dns, int port);
    void connectionToTracker(std::string login, std::string password);
    void getListOfVideo();
    int  isConnected();

  private:
    sf::SocketTCP client;
    std::string token;
};

#endif /* !CLIENT__HH__ */
