#include "client.hpp"

Client::Client(std::string dns, int port) {
    if (client.Connect(port, dns) != sf::Socket::Done) {
        std::cerr << "Unable to connect at " << dns << std::endl;
    }
    std::cerr << "Connect at " << dns << " with " << port << std::endl;
}

void
Client::connectionToTracker(std::string login, std::string password) {
    sf::Packet packet;
    sf::Uint16 opcode;
    sf::Int16 i;
    unsigned char hash[20];
    char* pass = new char[41];
    pass[41] = 0;


    std::cout << "Authentification [" << login << ":" << password << "]" << std::endl;

    sha1::calc(password.c_str(), password.length(), hash);
    sha1::toHexString(hash, pass);
    password = pass;
    std::cout << "Sha1 [" << password << "]" << std::endl;

    i = 0;
    opcode = MERGE_OPCODE(ConnexionType::CLIENT_TRACKER, CT::CONN_MASTER);
    packet << opcode;
    packet << login << password << "127.0.0.1" << i;

    if (client.Send(packet) != sf::Socket::Done) {
        std::cerr << "Unable to send connection" << std::endl;
        return;
    }
    
    std::cout << "En attente" << std::endl;
    if (client.Receive(packet) != sf::Socket::Done) {
        std::cerr << "Error during receiving." << std::endl;
        return;
    }

    packet >> opcode;
    if (EXTRACT_TYPE(opcode) != ConnexionType::TRACKER_CLIENT) {
        std::cerr << "Reception d'un opcode du movais type: ";
        std::cerr << EXTRACT_TYPE(opcode) << std::endl;
        return;
    }

    if (EXTRACT_CODE(opcode) != TC::TOKEN) {
        std::cerr << "Reception d'un opcode du movais code: ";
        std::cerr << EXTRACT_CODE(opcode) << std::endl;
        return;
    }

    packet >> token;
    std::cout << "Token: " << token << std::endl;
}

void
Client::getListOfVideo() {
    sf::Packet packet;
    sf::Uint16 opcode;
    sf::Uint8 filtre = 0;

    opcode = MERGE_OPCODE(ConnexionType::CLIENT_TRACKER, CT::ASK_LIST);
    packet << opcode;
    packet << filtre << "*";

    if (client.Send(packet) != sf::Socket::Done) {
        std::cerr << "Unable to send connection" << std::endl;
        return;
    }

    if (client.Receive(packet) != sf::Socket::Done) {
        std::cerr << "Error during receiving." << std::endl;
        return;
    }

    packet >> opcode;
    if (EXTRACT_TYPE(opcode) != ConnexionType::TRACKER_CLIENT) {
        std::cerr << "Reception d'un opcode du movais type: ";
        std::cerr << EXTRACT_TYPE(opcode) << std::endl;
        return;
    }

    if (EXTRACT_CODE(opcode) != TC::LIST) {
        std::cerr << "Reception d'un opcode du movais code: ";
        std::cerr << EXTRACT_CODE(opcode) << std::endl;
        return;
    }
    
    sf::Int32 count;
    packet >> count;
    std::cout << "Reception de " << count << " videos" << std::endl;
    for (int i = 0; i < count; i++) {
        std::string name;
        sf::Int32 id;
        packet >> name;
        packet >> id;
        std::cerr << "Name: " << name << " id: " << id  << std::endl;
    }

}

int
Client::isConnected() {
    return 1;
}
