#include <iostream>

#include "client.hpp"

#define HOST "37.59.85.217"
#define HOSTDNS "vs.kenlodin.fr"

int main (int argc, char **argv)
{
  if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " login password" << std::endl;
    return 1;
  }

  std::cout << "Connection"<< std::endl;
  Client* client = new Client(HOST, 36000);
  std::cout << "Authentification"<< std::endl;
  client->connectionToTracker(argv[1], argv[2]);
  std::cout << "Get list of video"<< std::endl;
  client->getListOfVideo();
  return 0;
}
