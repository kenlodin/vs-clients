/* 
 * File:   SharedStuff.cpp
 * Author: kokarez
 * 
 * Created on 23 avril 2012, 22:00
 */

#include "SharedStuff.h"
# include <unistd.h>

SharedStuff * SharedStuff::instance_ = NULL;
SharedStuff *SharedStuff::getInstance()
{
    if (instance_ != NULL)
        return instance_;
    instance_ = new SharedStuff();
    return instance_;
}

SharedStuff::SharedStuff()
{
    
}