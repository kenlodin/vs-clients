/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenlodin.vs.worker;

import android.content.Intent;
import android.net.Uri;

import com.kenlodin.vs.controller.DebugController;
import com.kenlodin.vs.controller.MainController;
//import controller.FilmController;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.kenlodin.vs.network.SharedStuff;

/**
 *
 * @author MMO POWA
 */
public class FilmWriter implements Runnable {

    File tmp;
    Integer numberNeeded = 0;
    Integer notFountCount = 0;
    OutputStream out;

    public void checkNextInList() {

        byte[] bytes = null;

        synchronized (SharedStuff.bytePoll) {
            if (SharedStuff.bytePoll.containsKey(numberNeeded)) {
                bytes = SharedStuff.bytePoll.get(numberNeeded);
                SharedStuff.bytePoll.remove(numberNeeded);
                numberNeeded++;
                notFountCount = 0;
            }
            else
                notFountCount++;
            if (notFountCount > 1000 && numberNeeded != 0) {
                notFountCount = 0;
                DebugController.GetInstance().println("Chunk num:" + numberNeeded + " was not Found");
            }
                
        }
        if (bytes != null) {
            if (0 == (numberNeeded % 100)) {
                DebugController.GetInstance().println("Write chunk num:" + numberNeeded);
            }
            try {
                write(bytes);
            } catch (IOException ex) {
                Logger.getLogger(FilmWriter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void write(byte[] bytes) throws FileNotFoundException, IOException {

        // Create the file 
        if (numberNeeded == 1) {
            tmp = File.createTempFile("filmvs", ".tmp");
            tmp.deleteOnExit();
            DebugController.GetInstance().println("File temp " + tmp.getCanonicalPath());
            out = new FileOutputStream(tmp);
        }

        out.write(bytes);
        out.flush();
        
        if (numberNeeded == 400) {
        		MainController.GetInstance().setFilm(tmp.getCanonicalPath());
        }

    }

    public void run() {

        while (true) {

            checkNextInList();

            try {
                Thread.currentThread();
				Thread.sleep(1);
                if (numberNeeded == 0) {
                    Thread.currentThread();
					Thread.sleep(100);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FilmWriter.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
