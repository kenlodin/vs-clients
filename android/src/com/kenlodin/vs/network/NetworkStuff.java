/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenlodin.vs.network;

import com.kenlodin.vs.controller.DebugController;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MMO POWA
 */
public class NetworkStuff {
	
    public static void TrackerConnection(Socket socket) {

        synchronized (SharedStuff.socketsTracker) {
            SharedStuff.socketsTracker.add(socket);
        }

        //LoginController.getInstance().eventPerfomed(LoginController.Info.ENABLE_LOGIN);
    }

    public static void TrackerDeconnection(Socket socket) {

        DebugController.GetInstance().println("TrackerDeconnection");

        synchronized (SharedStuff.socketsTracker) {
            if (SharedStuff.socketsTracker.contains(socket)) {
                SharedStuff.socketsTracker.remove(socket);
            }
        }
    }

    public static void DiffusionConnection(Socket socket) {

        synchronized (SharedStuff.socketsDiffusion) {
            SharedStuff.socketsDiffusion.add(socket);
        }

        //LoginController.getInstance().eventPerfomed(LoginController.Info.ENABLE_LOGIN);
    }

    public static void DiffusionDeconnection(Socket socket) {

        DebugController.GetInstance().println("DiffusionDeconnection");

        synchronized (SharedStuff.socketsDiffusion) {
            if (SharedStuff.socketsDiffusion.contains(socket)) {
                SharedStuff.socketsDiffusion.remove(socket);
            }
        }
    }

    public static void killAll() {

        DebugController.GetInstance().println("Kill all actual tracker/diffusion servers");
        synchronized (SharedStuff.socketsTracker) {
            for (Socket socket : SharedStuff.socketsTracker) {
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(NetworkStuff.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SharedStuff.socketsTracker.clear();
        }

        synchronized (SharedStuff.socketsDiffusion) {
            for (Socket socket : SharedStuff.socketsDiffusion) {
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(NetworkStuff.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SharedStuff.socketsDiffusion.clear();
        }
    }

    public static boolean verifyConnection() {

        boolean res = true;
        synchronized (SharedStuff.socketsDiffusion) {
            synchronized (SharedStuff.socketsTracker) {
                res = !(SharedStuff.socketsDiffusion.isEmpty() && SharedStuff.socketsTracker.isEmpty());
            }
        }
        return res;
    }
    
    public static void pushOnTheStack(int numbre, byte[] data) {
        
       /* Byte[] bytes = new Byte[data.length];
        
        for (int i = 0; i < data.length; i++) {
            bytes[i] = new Byte(data[i]);
        }*/
        //DebugController.getInstance().println("-> cdData size:" + data.length + " packet num:" + numbre);
        synchronized (SharedStuff.bytePoll) {
            SharedStuff.bytePoll.put(new Integer(numbre), data);
        }
        
    }
}
