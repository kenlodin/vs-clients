/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kenlodin.vs.network.sfml;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author kokarez
 */
public class SfmlNetworkWrapper {

    Socket socket;
    DataInputStream input;

    public SfmlNetworkWrapper(String host, int port) throws IOException {
        //DebugController.getInstance().println("Ouverture de la connexion: " + host);
        socket = new Socket(host, port);
        if (!socket.isConnected())
        {
            return;
        }
        input = new DataInputStream(socket.getInputStream());
        //DebugController.getInstance().println("Connexion ouverte");
    }

    public SfmlPacket Receive() throws IOException, SfmlPacketException {
        return new SfmlPacket(input);
    }

    public Socket getSocket() {
        return socket;
    }
    
    public void close() throws IOException {
        socket.close();
    }

}
