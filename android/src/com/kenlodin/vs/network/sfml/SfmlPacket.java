/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kenlodin.vs.network.sfml;

import java.io.*;
import java.net.Socket;

/**
 *
 * @author kokarez
 */
public class SfmlPacket {

    int size;
    
    /************************************/
    /*        In packet                 */
    /************************************/
    
    int read_pos;
    DataInputStream input;

    private boolean checkSize(int n) {
        return (size >= read_pos + n);
    }

    public SfmlPacket(DataInputStream input) throws IOException, SfmlPacketException {
        this.size = input.readInt();
        this.input = input;
        this.read_pos = 0;
    }

    public int getInt8() throws IOException, SfmlPacketException {
        if (!checkSize(1))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 1;
        return input.readByte();
    }

    public int getInt16() throws IOException, SfmlPacketException {
        if (!checkSize(2))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 2;
        return input.readShort();
    }

    public int getInt32() throws IOException, SfmlPacketException {
        if (!checkSize(4))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 4;
        return input.readInt();
    }

    public int getUInt8() throws IOException, SfmlPacketException {
        if (!checkSize(1))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 1;
        return input.readUnsignedByte();
    }

    public int getUInt16() throws IOException, SfmlPacketException {
        if (!checkSize(2))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 2;
        return input.readUnsignedShort();
    }

    public int getUInt32() throws IOException, SfmlPacketException {
        if (!checkSize(4))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 4;
        return input.readInt();
    }

    public byte[] getData() throws IOException, SfmlPacketException {
        byte b[];
        int end = this.size - this.read_pos;
        int bsize = this.size - this.read_pos;
        int reading = 0;

        b = new byte[bsize];
        if (!checkSize(bsize))
        {
            System.out.println("Erreur de lecture de packet: " + bsize);
            return null;
        }
        while ((reading += input.read(b, reading, bsize)) != end)
        {
            bsize = end - reading;
            //System.out.println("getString(getData = " + end + " != " + reading + ")");
        }
        this.read_pos += bsize;
        return b;
    }
    
    public String getString() throws IOException, SfmlPacketException {
        int str_size;
        byte b[];

        str_size = getInt32();
        b = new byte[str_size];
        if (!checkSize(str_size))
        {
            System.out.println("Erreur de lecture de packet: " + str_size);
            return new String();
        }
        if (input.read(b, 0, str_size) != str_size)
        {
            System.out.println("Erreur de lecture de packet");
            System.out.println("getString(size = " + size + ")");
        }
        this.read_pos += str_size;
        return new String(b);
    }

    /************************************/
    /*        out packet                */
    /************************************/
    
    ByteArrayOutputStream   out;
    DataOutputStream        dout;
    public SfmlPacket() {
        size = 0;
        out = new ByteArrayOutputStream();
        dout = new DataOutputStream(out);
    }

    public void setUInt8(long i) throws IOException {
        size += 1;
        dout.writeByte((int)i & 0xFF);
    }
    
    public void setUInt16(long i) throws IOException {
        size += 2;
        dout.writeShort((int)i & 0xFFFF);
    }
    
    public void setUInt32(long i) throws IOException {
        size += 4;
        dout.writeInt((int)i);
    }
    
    public void setInt8(int i) throws IOException {
        size += 1;
        dout.writeByte(i);
    }
        
    public void setInt16(int i) throws IOException {
        size += 2;
        dout.writeShort(i);
    }
    
    public void setInt32(int i) throws IOException {
        size += 4;
        dout.writeInt(i);
    }
    
    public void setString(String str) throws IOException {
        size += str.length();
        setInt32(str.length());
        for (int i = 0; i < str.length(); i++) {
            dout.writeByte(str.charAt(i));
        }
    }
    
    public void send(Socket socket) throws IOException {
        if (socket.isClosed())
            System.out.println("socket is closed");
        DataOutputStream ow = new DataOutputStream(socket.getOutputStream());
        ow.writeInt(size);
        byte[] b = out.toByteArray();
        
        for (int i = 0; i < size; i++) {
            ow.writeByte(b[i]);
        }
        
        ow.flush();
    }
}
