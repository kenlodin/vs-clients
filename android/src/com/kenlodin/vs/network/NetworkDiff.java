/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenlodin.vs.network;

import com.kenlodin.vs.Utils;
import com.kenlodin.vs.controller.DebugController;
import com.kenlodin.vs.controller.MainController;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.kenlodin.vs.network.sfml.SfmlNetworkWrapper;
import com.kenlodin.vs.network.sfml.SfmlPacket;
import com.kenlodin.vs.network.sfml.SfmlPacketException;

/**
 *
 * @author MMO POWA
 */
public class NetworkDiff implements Runnable {

    SfmlNetworkWrapper sfml = null;
    String host;
    int port;
    SfmlPacket packet;

    public NetworkDiff(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void authentifieMyToken() {
        try {
            SfmlPacket pack = new SfmlPacket();

            DebugController.GetInstance().println("<- authentifieMyToken: [" + MainController.GetInstance().getToken() + "] to " + host);
            pack.setUInt16(Opcode.Get(Opcode.Type.CLIENT_DIFF, Opcode.CD.TOKEN));
            pack.setString(MainController.GetInstance().getToken());
            pack.send(sfml.getSocket());
        } catch (IOException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void receiveMsgError(SfmlPacket packet) throws SfmlPacketException, IOException {
        int nbr_msg = packet.getInt32();
        String msg = packet.getString();
        DebugController.GetInstance().println("-> Msg error lgth[" + msg.length() + "]  nbr(" + nbr_msg + ")");
        Utils.alertbox(null, "error", msg);
    }

    public void receivePacket(SfmlPacket packet) {
        try {
            int opcode = packet.getUInt16();

            if (Opcode.GetType(opcode) != Opcode.Type.DIFF_CLIENT) {
                DebugController.GetInstance().println("Error opcode diffusion: " + opcode);
            } else {
                int action = Opcode.GetAction(opcode);
                if (action == Opcode.DC.DATA) {
                    receiveData(packet);
                }
                if (action == Opcode.DC.MSG) {
                    receiveMsgError(packet);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        try {
            Boolean isFinish = false;
            try {
                DebugController.GetInstance().println("Diffusion connection ip:" + host + " port:" + port);
                sfml = new SfmlNetworkWrapper(host, port);
            } catch (IOException ex) {
                Utils.alertbox(null, "Error", "Unable connect to the server");
                return;
            }
            /*
             * register socket
             */

            NetworkStuff.DiffusionConnection(sfml.getSocket());
            authentifieMyToken();


            while (!isFinish && !sfml.getSocket().isClosed()) {
                packet = sfml.Receive();
                /*
                 * Traite le packet
                 */
                receivePacket(packet);

                synchronized (SharedStuff.killAll) {
                    isFinish = SharedStuff.killAll;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkStuff.DiffusionDeconnection(sfml.getSocket());
    }

    private void receiveData(SfmlPacket packet) {
        try {
            int dataType = packet.getInt32();
            int packetCount = 0;
            packetCount = packet.getInt32();
            NetworkStuff.pushOnTheStack(packetCount, packet.getData());
            ///DebugController.GetInstance().println("Packet"  + packetCount);
        } catch (IOException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
