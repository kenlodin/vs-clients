/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenlodin.vs.network;

import com.kenlodin.vs.Utils;
import com.kenlodin.vs.controller.DebugController;
import com.kenlodin.vs.controller.MainController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.kenlodin.vs.network.sfml.SfmlNetworkWrapper;
import com.kenlodin.vs.network.sfml.SfmlPacket;
import com.kenlodin.vs.network.sfml.SfmlPacketException;

/**
 *
 * @author MMO POWA
 */
public class NetworkTracker implements Runnable {

    SfmlNetworkWrapper sfml = null;
    String host;
    int port;
    SfmlPacket packet;

    public NetworkTracker(String host) {
        this.host = host;
        this.port = 36000;
    }

    public void receiveFluxList(SfmlPacket packet) throws SfmlPacketException, IOException {
    	
    	// Dont implement this function for lot things
        
        int count = packet.getInt32();
        CharSequence[] items = new CharSequence[count];
        int[] ids = new int[count];
        
        DebugController.GetInstance().println("-> Receive " + count + " film(s)");
        for (int i = 0; i < count; i++) {
        	items[i] = packet.getString();
        	ids[i] = packet.getInt32();
            DebugController.GetInstance().println("   Film name: " + items[i]);
            DebugController.GetInstance().println("   Des: " + packet.getString());
            //FilmModel film = new FilmModel(name, 0, "No descrition", id);
            //ListFilmController.getInstance().addFilm(film);
        }
		Utils.spinenrBox(items, ids);
    }
    
    public void receiveToken(SfmlPacket packet) throws SfmlPacketException, IOException {
        String token = packet.getString();
        DebugController.GetInstance().println("-> Token [" + token + "]");
        MainController.GetInstance().setToken(token);
    }
    
    public void receiveUrl(SfmlPacket packet) throws SfmlPacketException, IOException {
        String ip = packet.getString();
        String link = packet.getString();
        DebugController.GetInstance().println("http://" + ip + ":8000" + link);
        MainController.GetInstance().setFilm("http://" + ip + ":8000" + link);
    }
    
    public void receiveMsgError(SfmlPacket packet) throws SfmlPacketException, IOException {
        int nbr_msg = packet.getInt32();
        String msg = packet.getString();
        DebugController.GetInstance().println("-> Msg error lgth[" + msg.length() + "]  nbr(" + nbr_msg + ")");
        //ErrorView.error(null, msg);
    }
    
    public void receiveServDiff(SfmlPacket packet) throws SfmlPacketException, IOException {
        int count = packet.getUInt8();
        
        List<NetworkDiff> diff = new ArrayList<NetworkDiff>();
        List<NetworkTracker> trakers = new ArrayList<NetworkTracker>();
        
        DebugController.GetInstance().println("-> Receive " + count + " serveur(s)");
        for (int i = 0; i < count; i++) {
            String  ip = packet.getString();
            int     port = packet.getUInt16();
            diff.add(new NetworkDiff(ip, port));
            trakers.add(new NetworkTracker(ip));
            DebugController.GetInstance().println("    Serveur ip: " + ip + " port: " + port);
        }
        
        // Kill actuel tracker and diff server
        NetworkStuff.killAll();
        
        // Connect to the new tracket and diffusion tracker
        for (NetworkTracker t : trakers) {
            new Thread(t).start();
        }
        
        for (NetworkDiff d : diff) {
            new Thread(d).start();
        }
        
    }
    
    public void receivePacket(SfmlPacket packet) {
        try {
            int opcode = packet.getUInt16();
            
            if (Opcode.GetType(opcode) != Opcode.Type.TRACKER_CLIENT)
                DebugController.GetInstance().println("Error opcode type tracker: " + Opcode.GetType(opcode));
            else {
                int action = Opcode.GetAction(opcode);
                if (action == Opcode.TC.LIST)
                    receiveFluxList(packet);
                if (action == Opcode.TC.TOKEN)
                    receiveToken(packet);
                if (action == Opcode.TC.LIST_DIFF)
                    receiveServDiff(packet);
                if (action == Opcode.TC.MSG)
                    receiveMsgError(packet);
                if (action == Opcode.TC.URL)
                    receiveUrl(packet);
            }
        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void AuthentificationSlave() {

        DebugController.GetInstance().println("<- AuthSlave [" + MainController.GetInstance().getToken() + "]");
        SfmlPacket packet = new SfmlPacket();
        try {
            packet.setUInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.CONN_SLAVE));


            packet.setString(MainController.GetInstance().getToken());
            packet.send(sfml.getSocket());
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void run() {

        try {
            Boolean isFinish = false;
            try {
                DebugController.GetInstance().println("Tracker connection ip:" + host);
                sfml = new SfmlNetworkWrapper(host, port);
            } catch (IOException ex) {
                Utils.alertbox(null, "Connection", "Unable connect to the server");
                return;
            }
            /*
             * register socket
             */
            NetworkStuff.TrackerConnection(sfml.getSocket());
            if (MainController.GetInstance().isConnected())
                AuthentificationSlave();
            else
            	NetworkSend.Authentification("test", "xavier est un gros con");


            while (!isFinish && !sfml.getSocket().isClosed()) {
                packet = sfml.Receive();
                /*
                 * Traite le packet
                 */
                receivePacket(packet);
                
                synchronized (SharedStuff.killAll) {
                    isFinish = SharedStuff.killAll;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkStuff.TrackerDeconnection(sfml.getSocket());
    }
}
