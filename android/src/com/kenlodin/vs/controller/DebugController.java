package com.kenlodin.vs.controller;

import android.util.Log;
import android.widget.TextView;

public class DebugController {
	
	private static DebugController instance_ = null;
	
	private TextView view = null;
	
	public static DebugController GetInstance() {
		if (instance_ == null) {
			instance_ = new DebugController();
		}
		return instance_;
	}

	private DebugController() {
		
	}
	
	public void setTextView(TextView view) {
		this.view = null;
	}
	
	public void println(String str) {
		if (view != null) {
			view.setText(view.getText() + str);
		}
		Log.i("Debug", str);
	}

}
