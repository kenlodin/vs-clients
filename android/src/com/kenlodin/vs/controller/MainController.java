package com.kenlodin.vs.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.VideoView;

import com.kenlodin.vs.R;
import com.kenlodin.vs.network.NetworkSend;

public class MainController implements Runnable {
	
	private static MainController instance_ = null;
	
	private String token = null;
	private Activity activity = null;
	
	public static MainController GetInstance() {
		if (instance_ == null) {
			instance_ = new MainController();
		}
		return instance_;
	}
	
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	public void setFilm(final String path) {
		activity.runOnUiThread(new Runnable() {
			
			public void run() {

				
	        	Intent tostart = new Intent(Intent.ACTION_VIEW);
	        	tostart.setDataAndType(Uri.parse(path), "video/*");
	        	activity.startActivity(tostart);
	        	/*
				VideoView view = (VideoView) activity.findViewById(R.id.video);
				view.setVideoURI(Uri.parse("http://37.59.85.217:8000/movie/75.mp4"));
				view.requestFocus();
				view.start();
				*/
			}
		});
	}

	public void run() {
		// TODO Auto-generated method stub
		
	}

	public boolean isConnected() {
		return token != null;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		//NetworkSend.GetFlux(4);
		NetworkSend.GetFluxList(1, "*");
		this.token = token;
	}

}
