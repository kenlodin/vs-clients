package com.kenlodin.vs;

import com.kenlodin.vs.controller.DebugController;
import com.kenlodin.vs.network.NetworkSend;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Utils {
	
	private static Activity context_ = null;
	
	public static void setContext(Activity context) {
		context_ = context;
	}
	
	public static void alertbox(Context context, final String title, final String mymessage)
    {
		DebugController.GetInstance().println(mymessage);
		if (context_ != null)
			context_.runOnUiThread(new Runnable() {
				
				public void run() {

					new AlertDialog.Builder(context_)
					.setMessage(mymessage)
					.setTitle(title)
					.setCancelable(true)
					.setNeutralButton(android.R.string.cancel,
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton){}
					})
		       .show();
					
				}
			});
    }
	
	public static void spinenrBox(final CharSequence[] items, final int[] ids)
    {
		if (context_ != null)
			context_.runOnUiThread(new Runnable() {
				
				public void run() {


					AlertDialog.Builder builder = new AlertDialog.Builder(context_);
					builder.setTitle("Choose your film");
					builder.setItems(items, new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int item) {
					        //Toast.makeText(context_.getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
					    	NetworkSend.GetUrl(ids[item]);
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
    }
}
