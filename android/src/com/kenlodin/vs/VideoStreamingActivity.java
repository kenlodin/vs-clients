package com.kenlodin.vs;

import com.kenlodin.vs.controller.DebugController;
import com.kenlodin.vs.controller.MainController;
import com.kenlodin.vs.network.NetworkStuff;
import com.kenlodin.vs.network.NetworkTracker;
import com.kenlodin.vs.network.SharedStuff;
import com.kenlodin.vs.worker.FilmWriter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

public class VideoStreamingActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Utils.setContext(this);
        MainController.GetInstance().setActivity(this);
        DebugController.GetInstance().println("Launch Activity");
        //new Thread(new FilmWriter()).start();
        if (SharedStuff.socketsTracker.isEmpty())
        	new Thread(new NetworkTracker("vs.kenlodin.fr")).start();
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	// TODO Auto-generated method stub
    	if (keyCode == 4)
        {
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure do you want to exit?")
                   .setCancelable(false)
                   .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                    	   android.os.Process.killProcess(android.os.Process.myPid());
                       }
                   })
                   .setNegativeButton("No", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                       }
                   });
            AlertDialog alert = builder.create();
            alert.show();
        }
    	return true;
    }
}