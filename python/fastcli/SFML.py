import struct

class SFMLPacketWriter():

  def __init__(self):
    self.size = 0
    self.buffer = ""

  def SetUInt32(self, i):
    self.buffer += struct.pack(">I",i)

  def SetInt32(self, i):
    self.buffer += struct.pack(">i",i)

  def SetUInt16(self, i):
    self.buffer += struct.pack(">H",i)

  def SetInt16(self, i):
    self.buffer += struct.pack(">h",i)

  def SetUInt8(self, i):
    self.buffer += struct.pack(">B",i)

  def SetInt8(self, i):
    self.buffer += struct.pack(">b",i)

  def SetString(self, string):
    self.SetUInt32(len(string))
    self.buffer += string

  def Send(self, sock):
    self.buffer = struct.pack(">I", len(self.buffer)) + self.buffer
    #for c in self.buffer:
    #  print "%#X" %ord(c),
    sock.send(self.buffer)

class SFMLPacketReceive():

  def __init__(self, sock):
    print "Waiting size"
    size = struct.unpack(">I", sock.recv(4))[0]
    print "Recv: " + str(size)
    self.buffer = sock.recv(size)
    self.pos = 0

  def GetUInt16(self):
    (res,) = struct.unpack(">H", self.buffer[self.pos : self.pos + 2])
    self.pos += 2
    return res

  def GetUInt32(self):
    (res,) = struct.unpack(">I", self.buffer[self.pos : self.pos + 4])
    self.pos += 4
    return res

  def GetInt32(self):
    (res,) = struct.unpack(">i", self.buffer[self.pos : self.pos + 4])
    self.pos += 4
    return res

  def GetString(self):
    size = self.GetUInt32()
    res = self.buffer[self.pos : self.pos + size]
    self.pos += size
    return res
