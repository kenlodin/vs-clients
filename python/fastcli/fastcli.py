from socket import *
from SFML import *
import sys

sock = socket (AF_INET, SOCK_STREAM)
token = False

def connect():
  sock.connect (('37.59.85.217',36000))

def close():
  sock.close()

def ping():
  packet = SFMLPacketWriter()
  packet.SetUInt16(12)
  packet.Send(sock)
  packet = SFMLPacketReceive(sock)
  if (packet.GetUInt16() & 0xFFF) == 5:
    print "PING[OK]"
  else:
    print "PING[FAIL]"

def auth():
  packet = SFMLPacketWriter()
  packet.SetUInt16(0)
  packet.SetString("test")
  packet.SetString("0fd7bf4aef2dbe4e71ac2cc93e75ed02d9931822")
  packet.SetString("127.0.0.1")
  packet.SetInt16(0)
  packet.Send(sock)
  packet = SFMLPacketReceive(sock)
  packet.GetUInt16()
  token = packet.GetString() 
  print "Token[" + token + "]"
  
def getList():
  packet = SFMLPacketWriter()
  packet.SetUInt16(2)
  packet.SetUInt8(0)
  packet.SetString("*")
  packet.Send(sock)
  packet = SFMLPacketReceive(sock)
  packet.GetUInt16()
  count = packet.GetInt32()
  while count > 0:
    print "Film name:" + packet.GetString(),
    print " id: " + str(packet.GetInt32())
    count -= 1

def main():
  connect()

  for arg in sys.argv:
    if arg == sys.argv[0]:
      continue
    print "cmd: " + arg
    if arg == "ping" :
      ping()
    if arg == "auth" :
      auth()
    if arg == "list" :
      getList()

  close()

main()
