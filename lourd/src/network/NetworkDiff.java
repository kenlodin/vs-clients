/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import controller.DebugController;
import controller.LoginController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.sfml.SfmlNetworkWrapper;
import network.sfml.SfmlPacket;
import network.sfml.SfmlPacketException;
import view.ErrorView;

/**
 *
 * @author MMO POWA
 */
public class NetworkDiff implements Runnable {

    SfmlNetworkWrapper sfml = null;
    String host;
    int port;
    SfmlPacket packet;

    public NetworkDiff(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Authentifiate user on the diffusion server
     */
    public void authentifieMyToken() {
        try {
            SfmlPacket pack = new SfmlPacket();

            DebugController.getInstance().println("<- authentifieMyToken: [" + LoginController.getInstance().getToken() + "] to " + host);
            pack.setUInt16(Opcode.Get(Opcode.Type.CLIENT_DIFF, Opcode.CD.TOKEN));
            pack.setString(LoginController.getInstance().getToken());
            pack.send(sfml.getSocket());
        } catch (IOException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Receive and draw an error nessage
     * @param packet
     * @throws SfmlPacketException
     * @throws IOException 
     */
    public void receiveMsgError(SfmlPacket packet) throws SfmlPacketException, IOException {
        int nbr_msg = packet.getInt32();
        String msg = packet.getString();
        DebugController.getInstance().println("-> Msg error lgth[" + msg.length() + "]  nbr(" + nbr_msg + ")");
        ErrorView.error(null, msg);
    }
    
    /**
     * Method to dispatch received packet
     * @param packet 
     */
    public void receivePacket(SfmlPacket packet) {
        try {
            int opcode = packet.getUInt16();

            if (Opcode.GetType(opcode) != Opcode.Type.DIFF_CLIENT) {
                DebugController.getInstance().println("Error opcode diffusion: " + opcode);
            } else {
                int action = Opcode.GetAction(opcode);
                if (action == Opcode.DC.DATA) {
                    receiveData(packet);
                }
                if (action == Opcode.DC.MSG) {
                    receiveMsgError(packet);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        try {
            Boolean isFinish = false;
            try {
                DebugController.getInstance().println("Diffusion connection ip:" + host + " port:" + port);
                sfml = new SfmlNetworkWrapper(host, port);
            } catch (IOException ex) {
                ErrorView.error(null, "Unable connect to the server");
                return;
            }
            /*
             * register socket
             */

            NetworkStuff.DiffusionConnection(sfml.getSocket());
            authentifieMyToken();


            while (!isFinish && !sfml.getSocket().isClosed()) {
                packet = sfml.Receive();
                /*
                 * Traite le packet
                 */
                receivePacket(packet);

                synchronized (SharedStuff.killAll) {
                    isFinish = SharedStuff.killAll;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkStuff.DiffusionDeconnection(sfml.getSocket());
    }

    /**
     * Receive data packet
     * @param packet 
     */
    private void receiveData(SfmlPacket packet) {
        try {
            int dataType = packet.getInt32();
            int packetCount = 0;
            packetCount = packet.getInt32();
            NetworkStuff.pushOnTheStack(packetCount, packet.getData());
        } catch (IOException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkDiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
