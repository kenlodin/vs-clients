/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import controller.DebugController;
import controller.LoginController;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.sfml.SfmlPacket;

/**
 *
 * @author MMO POWA
 */
public class NetworkSend {

    /**
     * Make a Sha1 on the string passing in argument
     * @param string to sha1
     * @return sha1 string
     */
    public static String Sha1(String pass) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(pass.getBytes());
            StringBuffer hashString = new StringBuffer();
            for (int i = 0; i < digest.length; ++i) {
                String hex = Integer.toHexString(digest[i]);
                if (hex.length() == 1) {
                    hashString.append('0');
                    hashString.append(hex.charAt(hex.length() - 1));
                } else {
                    hashString.append(hex.substring(hex.length() - 2));
                }
            }
            return hashString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * Send authentification to a tracker
     * @param login
     * @param password 
     */
    public static void Authentification(String login, String password) {

        SfmlPacket packet = new SfmlPacket();
        try {
            packet.setUInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.CONN_MASTER));

            String toString = Sha1(password);
            DebugController.getInstance().println("<- Auth [" + login + ":" + toString + "]");

            packet.setString(login);
            packet.setString(toString);
            packet.setString("127.0.0.1");
            packet.setInt16(0);
            Send(packet);
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Ask the list of available video
     * @param filtre
     * @param list 
     */
    public static void GetFluxList(int filtre, String list) {
        DebugController.getInstance().println("<- FluxList [" + filtre + ":" + list + "]");
        try {
            SfmlPacket packet = new SfmlPacket();

            packet.setInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.ASK_LIST));
            packet.setUInt8(filtre);
            packet.setString(list);
            Send(packet);
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Ask film
     * @param video 
     */
    public static void GetFlux(int video) {
        DebugController.getInstance().println("<- Flux [" + video + "]");
        try {
            SfmlPacket packet = new SfmlPacket();

            packet.setInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.ASK_FLUX));
            packet.setUInt32(video);
            Send(packet);
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Ask to server to stop film sending
     */
    public static void StopFlux() {
        DebugController.getInstance().println("<- Flux stop");
        try {
            SfmlPacket packet = new SfmlPacket();

            packet.setInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.ASK_STOP));
            Send(packet);
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Send a packet on a tracker
     * @param packet
     * @throws IOException 
     */
    public static void Send(SfmlPacket packet) throws IOException {

        synchronized (SharedStuff.socketsTracker) {
            packet.send(SharedStuff.socketsTracker.get(0));
        }
    }
}
