/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network.sfml;

import java.io.*;
import java.net.Socket;

/**
 *
 * @author kokarez
 */
public class SfmlPacket {

    int size;
    
    /************************************/
    /*        In packet                 */
    /************************************/
    
    int read_pos;
    DataInputStream input;

    /**
     * Check if the size given in argument is correct
     * @param n
     * @return 
     */
    private boolean checkSize(int n) {
        return (size >= read_pos + n);
    }

    public SfmlPacket(DataInputStream input) throws IOException, SfmlPacketException {
        this.size = input.readInt();
        this.input = input;
        this.read_pos = 0;
    }

    /**
     * Get Integer of 8bit
     * @return 8bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getInt8() throws IOException, SfmlPacketException {
        if (!checkSize(1))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 1;
        return input.readByte();
    }
    
    /**
     * Get Integer of 8bit
     * @return 8bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getInt16() throws IOException, SfmlPacketException {
        if (!checkSize(2))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 2;
        return input.readShort();
    }
    
    /**
     * Get Integer of 32bit
     * @return 32bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getInt32() throws IOException, SfmlPacketException {
        if (!checkSize(4))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 4;
        return input.readInt();
    }
    
    /**
     * Get Unsigned Integer of 8bit
     * @return 8bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getUInt8() throws IOException, SfmlPacketException {
        if (!checkSize(1))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 1;
        return input.readUnsignedByte();
    }
    
    /**
     * Get Unsigned Integer of 16bit
     * @return 16bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getUInt16() throws IOException, SfmlPacketException {
        if (!checkSize(2))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 2;
        return input.readUnsignedShort();
    }
    
    /**
     * Get Unsigned Integer of 32bit
     * @return 32bit
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public int getUInt32() throws IOException, SfmlPacketException {
        if (!checkSize(4))
        {
            System.out.println("Erreur de lecture de packet");
            return 0;
        }
        this.read_pos += 4;
        return input.readInt();
    }

    /**
     * Get finish data
     * @return data
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public byte[] getData() throws IOException, SfmlPacketException {
        byte b[];
        int end = this.size - this.read_pos;
        int bsize = this.size - this.read_pos;
        int reading = 0;

        b = new byte[bsize];
        if (!checkSize(bsize))
        {
            System.out.println("Erreur de lecture de packet: " + bsize);
            return null;
        }
        while ((reading += input.read(b, reading, bsize)) != end)
        {
            bsize = end - reading;
            //System.out.println("getString(getData = " + end + " != " + reading + ")");
        }
        this.read_pos += bsize;
        return b;
    }
    
    /**
     * Get string on the packet
     * @return string
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public String getString() throws IOException, SfmlPacketException {
        int str_size;
        byte b[];

        str_size = getInt32();
        b = new byte[str_size];
        if (!checkSize(str_size))
        {
            System.out.println("Erreur de lecture de packet: " + str_size);
            return new String();
        }
        if (input.read(b, 0, str_size) != str_size)
        {
            System.out.println("Erreur de lecture de packet");
            System.out.println("getString(size = " + size + ")");
        }
        this.read_pos += str_size;
        return new String(b);
    }

    /************************************/
    /*        out packet                */
    /************************************/
    
    ByteArrayOutputStream   out;
    DataOutputStream        dout;
    public SfmlPacket() {
        size = 0;
        out = new ByteArrayOutputStream();
        dout = new DataOutputStream(out);
    }

    /**
     * Add an Unsigned int of 8bit in packet
     * @param 8bit
     * @throws IOException 
     */
    public void setUInt8(long i) throws IOException {
        size += 1;
        dout.writeByte((int)i & 0xFF);
    }
    
    /**
     * Add an Unsigned int of 16bit in packet
     * @param 16bit
     * @throws IOException 
     */
    public void setUInt16(long i) throws IOException {
        size += 2;
        dout.writeShort((int)i & 0xFFFF);
    }
    
    /**
     * Add an Unsigned int of 32bit in packet
     * @param 32bit
     * @throws IOException 
     */
    public void setUInt32(long i) throws IOException {
        size += 4;
        dout.writeInt((int)i);
    }
    
    /**
     * Add an int of 8bit in packet
     * @param 8bit
     * @throws IOException 
     */
    public void setInt8(int i) throws IOException {
        size += 1;
        dout.writeByte(i);
    }
        
    /**
     * Add an int of 16bit in packet
     * @param 16bit
     * @throws IOException 
     */
    public void setInt16(int i) throws IOException {
        size += 2;
        dout.writeShort(i);
    }
    
    /**
     * Add an int of 32bit in packet
     * @param 32bit
     * @throws IOException 
     */
    public void setInt32(int i) throws IOException {
        size += 4;
        dout.writeInt(i);
    }
    
    /**
     * Add string on packet
     * @param string
     * @throws IOException 
     */
    public void setString(String str) throws IOException {
        size += str.length();
        setInt32(str.length());
        for (int i = 0; i < str.length(); i++) {
            dout.writeByte(str.charAt(i));
        }
    }
    
    /**
     * Send the packet on the given socket
     * @param socket
     * @throws IOException 
     */
    public void send(Socket socket) throws IOException {
        if (socket.isClosed())
            System.out.println("socket is closed");
        DataOutputStream ow = new DataOutputStream(socket.getOutputStream());
        ow.writeInt(size);
        byte[] b = out.toByteArray();
        
        for (int i = 0; i < size; i++) {
            ow.writeByte(b[i]);
        }
        
        ow.flush();
    }
}
