/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network.sfml;

import controller.DebugController;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author kokarez
 */
public class SfmlNetworkWrapper {

    Socket socket;
    DataInputStream input;

    /**
     * Connect to the server with the host anf port
     * @param host
     * @param port
     * @throws IOException 
     */
    public SfmlNetworkWrapper(String host, int port) throws IOException {
        //DebugController.getInstance().println("Ouverture de la connexion: " + host);
        socket = new Socket(host, port);
        if (!socket.isConnected())
        {
            DebugController.getInstance().println("Erreur de connexion");
            return;
        }
        input = new DataInputStream(socket.getInputStream());
        //DebugController.getInstance().println("Connexion ouverte");
    }

    /**
     * Contruct an SFML packet
     * @return SFML packet
     * @throws IOException
     * @throws SfmlPacketException 
     */
    public SfmlPacket Receive() throws IOException, SfmlPacketException {
        return new SfmlPacket(input);
    }

    /**
     * Return the current socket of the connection
     * @return current socket
     */
    public Socket getSocket() {
        return socket;
    }
    
    /**
     * Close the connexion
     * @throws IOException 
     */
    public void close() throws IOException {
        socket.close();
        DebugController.getInstance().println("Fermeture de la connexion");
    }

}
