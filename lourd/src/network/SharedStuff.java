/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.Socket;
import java.util.*;

/**
 *
 * @author MMO POWA
 */
public class SharedStuff {
    
    /**
     * List of current tracker server connected
     */
    public static List<Socket> socketsTracker = new ArrayList<Socket>();
    
    /**
     * List of current diffusion server connected
     */
    public static List<Socket> socketsDiffusion = new ArrayList<Socket>();
    
    public static HashMap<Integer, Byte[]> bytesPool = new HashMap<Integer, Byte[]>();
    public static HashMap<Integer, byte[]> bytePoll = new HashMap<Integer, byte[]>();
    
    public static Boolean killAll = false;
    
}
