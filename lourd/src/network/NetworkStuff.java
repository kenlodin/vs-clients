/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import controller.DebugController;
import controller.LoginController;
import java.io.IOException;
import java.net.Socket;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MMO POWA
 */
public class NetworkStuff {

    /**
     * Save socket of the new tracker connection
     * @param socket 
     */
    public static void TrackerConnection(Socket socket) {

        synchronized (SharedStuff.socketsTracker) {
            SharedStuff.socketsTracker.add(socket);
        }

        LoginController.getInstance().eventPerfomed(LoginController.Info.ENABLE_LOGIN);
    }

    /**
     * Delete socket of the tracker server
     * @param socket 
     */
    public static void TrackerDeconnection(Socket socket) {

        DebugController.getInstance().println("TrackerDeconnection");

        synchronized (SharedStuff.socketsTracker) {
            if (SharedStuff.socketsTracker.contains(socket)) {
                SharedStuff.socketsTracker.remove(socket);
            }
        }
    }

    /**
     * Put all socket on new list and clear other list. New list is returned.
     * @return New list
     */
    public static List<Socket> get_all_socket() {

        List<Socket> res = new ArrayList<Socket>();
        
        synchronized (SharedStuff.socketsDiffusion) {
            for (int i = 0; i < SharedStuff.socketsDiffusion.size(); i++)
                res.add(SharedStuff.socketsDiffusion.get(i));
            SharedStuff.socketsDiffusion.clear();
        }

        synchronized (SharedStuff.socketsTracker) {
            for (int i = 0; i < SharedStuff.socketsTracker.size(); i++)
                res.add(SharedStuff.socketsTracker.get(i));
            SharedStuff.socketsTracker.clear();
        }

        return res;
    }
    
    /**
     * Save socket of the new diffusion connection
     * @param socket 
     */
    public static void DiffusionConnection(Socket socket) {

        synchronized (SharedStuff.socketsDiffusion) {
            SharedStuff.socketsDiffusion.add(socket);
        }

        LoginController.getInstance().eventPerfomed(LoginController.Info.ENABLE_LOGIN);
    }

    /**
     * Delete socket of the diffusion server
     * @param socket 
     */
    public static void DiffusionDeconnection(Socket socket) {

        DebugController.getInstance().println("DiffusionDeconnection");

        synchronized (SharedStuff.socketsDiffusion) {
            if (SharedStuff.socketsDiffusion.contains(socket)) {
                SharedStuff.socketsDiffusion.remove(socket);
            }
        }
    }

    /**
     * Close all socket
     */
    public static void killAll() {

        DebugController.getInstance().println("Kill all actual tracker/diffusion servers");
        synchronized (SharedStuff.socketsTracker) {
            for (Socket socket : SharedStuff.socketsTracker) {
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(NetworkStuff.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SharedStuff.socketsTracker.clear();
        }

        synchronized (SharedStuff.socketsDiffusion) {
            for (Socket socket : SharedStuff.socketsDiffusion) {
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(NetworkStuff.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SharedStuff.socketsDiffusion.clear();
        }
    }

    /**
     * Check if connection on tracker ans diffusion server is open
     * @return state
     */
    public static boolean verifyConnection() {

        boolean res = true;
        synchronized (SharedStuff.socketsDiffusion) {
            synchronized (SharedStuff.socketsTracker) {
                res = !(SharedStuff.socketsDiffusion.isEmpty() && SharedStuff.socketsTracker.isEmpty());
            }
        }
        return res;
    }
    
    /**
     * Push on the writerFilm traitement list new packet
     * @param numbre
     * @param data 
     */
    public static void pushOnTheStack(int numbre, byte[] data) {
        
       /* Byte[] bytes = new Byte[data.length];
        
        for (int i = 0; i < data.length; i++) {
            bytes[i] = new Byte(data[i]);
        }*/
        //DebugController.getInstance().println("-> cdData size:" + data.length + " packet num:" + numbre);
        synchronized (SharedStuff.bytePoll) {
            SharedStuff.bytePoll.put(new Integer(numbre), data);
        }
        
    }
}
