/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

/**
 *
 * @author MMO POWA
 */
public class Opcode {

    public class Type {

        public final static int CLIENT_TRACKER = 0;
        public final static int TRACKER_CLIENT = 1;
        public final static int CLIENT_DIFF = 2;
        public final static int DIFF_CLIENT = 3;
    }
    
    public class TypeCDData {
        
        public final static int AVI_RIFF_AVI = 0;
        public final static int AVI_LIST_HDRL = 1;
        public final static int AVI_LIST_INFO = 2;
        public final static int AVI_JUNK = 3;
        public final static int AVI_LIST_MOVI = 4;
        public final static int AVI_CHUNK = 5;
        
    }

    public class CT {

        public final static int CONN_MASTER = 0;
        public final static int CONN_SLAVE = 1;
        public final static int ASK_LIST = 2;
        public final static int ASK_FLUX = 3;
        public final static int ASK_CHECK = 4;
        public final static int ASK_PACKET = 5;
        public final static int ASK_RPACKET = 6;
        public final static int ASK_MOVE = 7;
        public final static int ASK_DEFICIENT = 8;
        public final static int ASK_REM = 9;
        public final static int ASK_STOP = 10;
        public final static int DEC = 11;
    }

    public class TC {

        public final static int TOKEN = 0;
        public final static int LIST = 1;
        public final static int LIST_DIFF = 2;
        public final static int LIST_NDIFF = 3;
        public final static int MSG = 4;
    }

    public class CD {

        public final static int TOKEN = 0;
    }

    public class DC {

        public final static int DATA = 0;
        public final static int MSG = 1;
    }

    public static int Get(int type, int code) {
        return (type << 12) + code;
    }
    
    public static int GetType(int opcode) {
        return opcode >> 12;
    }
    
    public static int GetAction(int opcode) {
        return opcode & 0xFFF;
    }
}
