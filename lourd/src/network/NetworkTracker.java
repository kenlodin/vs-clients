/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import controller.DebugController;
import controller.FilmController;
import controller.ListFilmController;
import controller.LoginController;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.FilmModel;
import network.sfml.SfmlNetworkWrapper;
import network.sfml.SfmlPacket;
import network.sfml.SfmlPacketException;
import view.ErrorView;
import worker.FilmWriter;

/**
 *
 * @author MMO POWA
 */
public class NetworkTracker implements Runnable {

    SfmlNetworkWrapper sfml = null;
    String host;
    int port;
    SfmlPacket packet;

    public NetworkTracker(String host) {
        this.host = host;
        this.port = 36000;
    }

    /**
     * Receive the list of availible video on the server, and give it on the controller
     * @param packet
     * @throws SfmlPacketException
     * @throws IOException 
     */
    public void receiveFluxList(SfmlPacket packet) throws SfmlPacketException, IOException {
        int count = packet.getInt32();
        
        DebugController.getInstance().println("-> Receive " + count + " film(s)");
        for (int i = 0; i < count; i++) {
            String  name = packet.getString();
            int     id = packet.getInt32();
            String  des = packet.getString();
            DebugController.getInstance().println("   Film name: " + name + " id:" + id + " des:" + des);
            FilmModel film = new FilmModel(name, 0, des, id);
            ListFilmController.getInstance().addFilm(film);
        }
    }
    
    /**
     * Receive the token and set it
     * @param packet
     * @throws SfmlPacketException
     * @throws IOException 
     */
    public void receiveToken(SfmlPacket packet) throws SfmlPacketException, IOException {
        String token = packet.getString();
        DebugController.getInstance().println("-> Token [" + token + "]");
        LoginController.getInstance().setToken(token);
    }
    
    /**
     * Receive and draw an error nessage
     * @param packet
     * @throws SfmlPacketException
     * @throws IOException 
     */
    public void receiveMsgError(SfmlPacket packet) throws SfmlPacketException, IOException {
        int nbr_msg = packet.getInt32();
        String msg = packet.getString();
        DebugController.getInstance().println("-> Msg error lgth[" + msg.length() + "]  nbr(" + nbr_msg + ")");
        ErrorView.error(null, msg);
    }
    
    /**
     * Receive list of diffusion server to received film
     * @param packet
     * @throws SfmlPacketException
     * @throws IOException 
     */
    public void receiveServDiff(SfmlPacket packet) throws SfmlPacketException, IOException {
        int count = packet.getUInt8();
        
        List<NetworkDiff> diff = new ArrayList<NetworkDiff>();
        List<NetworkTracker> trakers = new ArrayList<NetworkTracker>();
        
        DebugController.getInstance().println("-> Receive " + count + " serveur(s)");
        for (int i = 0; i < count; i++) {
            String  ip = packet.getString();
            int     port = packet.getUInt16();
            diff.add(new NetworkDiff(ip, port));
            trakers.add(new NetworkTracker(ip));
            DebugController.getInstance().println("    Serveur ip: " + ip + " port: " + port);
        }
        
        // Kill actuel tracker and diff server
        //NetworkStuff.killAll();
        
        List<Socket> listdelete = NetworkStuff.get_all_socket();
        
        // Connect to the new tracket and diffusion tracker
        for (NetworkTracker t : trakers) {
            new Thread(t).start();
        }
        
        for (NetworkDiff d : diff) {
            new Thread(d).start();
        }
        
        //for (int i = 0; i < listdelete.size(); i++) {
        //    listdelete.get(i).close();
        //}
        
        FilmWriter.reset();
        
    }
    
    /**
     * Method to dispatch received packet
     * @param packet 
     */
    public void receivePacket(SfmlPacket packet) {
        try {
            int opcode = packet.getUInt16();
            
            if (Opcode.GetType(opcode) != Opcode.Type.TRACKER_CLIENT)
                DebugController.getInstance().println("Error opcode type tracker: " + Opcode.GetType(opcode));
            else {
                int action = Opcode.GetAction(opcode);
                if (action == Opcode.TC.LIST)
                    receiveFluxList(packet);
                if (action == Opcode.TC.TOKEN)
                    receiveToken(packet);
                if (action == Opcode.TC.LIST_DIFF)
                    receiveServDiff(packet);
                if (action == Opcode.TC.MSG)
                    receiveMsgError(packet);
            }
        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Send Authentification to the tracket when we have a token
     */
    public void AuthentificationSlave() {

        DebugController.getInstance().println("<- AuthSlave [" + LoginController.getInstance().getToken() + "]");
        SfmlPacket packet = new SfmlPacket();
        try {
            packet.setUInt16(Opcode.Get(Opcode.Type.CLIENT_TRACKER, Opcode.CT.CONN_SLAVE));


            packet.setString(LoginController.getInstance().getToken());
            packet.send(sfml.getSocket());
        } catch (IOException ex) {
            Logger.getLogger(NetworkSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run() {

        try {
            Boolean isFinish = false;
            try {
                DebugController.getInstance().println("Tracker connection ip:" + host);
                sfml = new SfmlNetworkWrapper(host, port);
            } catch (IOException ex) {
                ErrorView.error(null, "Unable connect to the server");
                return;
            }
            /*
             * register socket
             */
            if (LoginController.getInstance().isConnected())
                AuthentificationSlave();
            NetworkStuff.TrackerConnection(sfml.getSocket());


            while (!isFinish && !sfml.getSocket().isClosed()) {
                packet = sfml.Receive();
                /*
                 * Traite le packet
                 */
                receivePacket(packet);
                
                synchronized (SharedStuff.killAll) {
                    isFinish = SharedStuff.killAll;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SfmlPacketException ex) {
            Logger.getLogger(NetworkTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
        NetworkStuff.TrackerDeconnection(sfml.getSocket());
    }
}
