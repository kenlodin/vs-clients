/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author kokarez
 */
public class FilmModel {
    private String  name;
    private Integer type;
    private String description;
    private Integer id;

    public FilmModel(String name, Integer type, String description, Integer id) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
}
