/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kokarez
 */
public class FilmListModel extends AbstractTableModel {

    Vector<String> vectorTitre;
    Vector<FilmModel> vectorValue;

    public FilmListModel(Vector<String> vectorTitre, Vector<FilmModel> vectorValue) {
        this.vectorTitre = vectorTitre;
        this.vectorValue = vectorValue;
    }

    @Override
    public int getRowCount() {
        return vectorValue.size();
    }

    @Override
    public int getColumnCount() {
        return vectorTitre.size();
    }

    @Override
    public String getColumnName(int column) {
        return vectorTitre.get(column);
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0)
            return vectorValue.get(rowIndex).getName();
        if (columnIndex == 1)
            return vectorValue.get(rowIndex).getType();
        if (columnIndex == 2)
            return vectorValue.get(rowIndex).getDescription();
        return "";
    }

    /**
     * Add film on list
     * @param film new film
     */
    public void addFilm(FilmModel film) {
        vectorValue.add(film);
    }
    
    /**
     * return needed film on list by it id
     * @param row index id
     * @return film needed
     */
    public FilmModel getFilm(int row) {
        return vectorValue.get(row);
    }

}
