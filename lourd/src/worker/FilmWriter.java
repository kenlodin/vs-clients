/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worker;

import controller.DebugController;
import controller.FilmController;
import controller.MainController;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.SharedStuff;
import view.MainView;

/**
 *
 * @author MMO POWA
 */
public class FilmWriter implements Runnable {

    File tmp = null;
    Integer numberNeeded = 0;
    Integer notFountCount = 0;
    OutputStream out;
    static boolean reseting = false;

    /**
     * This method try to get the needed packet in the list to write it on
     * the file
     */
    public void checkNextInList() {

        byte[] bytes = null;

        synchronized (SharedStuff.bytePoll) {
            if (SharedStuff.bytePoll.containsKey(numberNeeded)) {
                bytes = SharedStuff.bytePoll.get(numberNeeded);
                SharedStuff.bytePoll.remove(numberNeeded);
                numberNeeded++;
                notFountCount = 0;
            }
            else
                notFountCount++;
            if (notFountCount > 1000 && numberNeeded != 0) {
                notFountCount = 0;
                DebugController.getInstance().println("Chunk num:" + numberNeeded + " was not Found");
            }
                
        }
        if (bytes != null) {
            if (0 == (numberNeeded % 100)) {
                DebugController.getInstance().println("Write chunk num:" + numberNeeded);
            }
            try {
                write(bytes);
            } catch (IOException ex) {
                Logger.getLogger(FilmWriter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Write necessary bytes on the current open file
     * @param bytes
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void write(byte[] bytes) throws FileNotFoundException, IOException {

        // Create the file 
        if (numberNeeded == 1) {
            tmp = File.createTempFile("filmvs", ".tmp");
            tmp.deleteOnExit();
            DebugController.getInstance().println("File temp " + tmp.getCanonicalPath());
            out = new FileOutputStream(tmp);
        }

        out.write(bytes);
        out.flush();
        
        if (numberNeeded == 400) {
            FilmController.getInstance().eventPerfomed(1, tmp.getCanonicalPath());
        }

    }

    /**
     * Ask at the thread to reset all settings, File, Heap ...
     */
    public static void reset() {
        reseting = true;
    }
    
    @Override
    public void run() {

        while (true) {

            if (reseting && tmp != null)
            {
                DebugController.getInstance().println("Reset mode");
                numberNeeded = 0;
                notFountCount = 0;
                tmp.delete();
                SharedStuff.bytePoll.clear();
            }
            reseting = false;
            if (MainController.getInstance().current_mode == MainController.Infos.FILM_DISPLAY)
                checkNextInList();

            try {
                Thread.currentThread().sleep(1);
                if (numberNeeded == 0) {
                    Thread.currentThread().sleep(100);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FilmWriter.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
