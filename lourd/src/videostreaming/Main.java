package videostreaming;

import controller.DebugController;
import controller.LoginController;
import javax.swing.UIManager;
import network.NetworkTracker;
import view.LoginView;
import worker.FilmWriter;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

public class Main {

    public static void main(String[] string) {

        //NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), "");
        //Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);

        if (string.length > 0) {
            if (string[0].equals("--debug")) {
                DebugController.getInstance().setDebug(Boolean.TRUE);
            }
            if (string[0].equals("--red")) {
                Redirect.redirect("myLogFile.log");
            }
        }

        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Unable to load Windows look and feel");
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new Thread(new FilmWriter()).start();
                new Thread(new NetworkTracker("vs.kenlodin.fr")).start();
                LoginController.getInstance().launch();
            }
        });
    }
}
