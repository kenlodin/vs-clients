/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package videostreaming;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 *
 * @author MMO POWA
 */
public class Redirect extends PrintStream {
    
    private static PrintStream instance;
    
    private Redirect(String filename) throws FileNotFoundException {
        super(filename);
    }
    
    /**
     * Redirect standart output on file
     * @param filename 
     */
    public static void redirect(String filename) {
        try {
            instance = new Redirect(filename);
            System.setErr(instance);
            System.setOut(instance);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return;
    }
    

}
