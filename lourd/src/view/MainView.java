/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.IController;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author MMO POWA
 */
public class MainView extends JFrame implements IView {

    IController controller;
    

    public MainView(IController controller) throws HeadlessException {
        this.controller = controller;
        initUI();
    }
    
    public final void initUI() {
        setTitle("Streamin video");
        setSize(800, 700);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    @Override
    public void Show() {
        setVisible(true);
    }

    @Override
    public void Hide() {
        setVisible(false);
    }

    @Override
    public Object getObject(String name) {
        if (name.equals("get_frame"))
            return this;
        return null;
    }
    
    JComponent old = null;
    
    @Override
    public void setMainPanel(JComponent panel) {
        if (old != null)
            remove(old);
        old = panel;
        add(panel);
        ViewTools.forceRepaint(panel);
    }

    @Override
    public JComponent getMainPanel() {
        throw new UnsupportedOperationException("MainView as no main Panel");
    }

    @Override
    public void action(Integer id, Object... objs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
