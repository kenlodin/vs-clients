/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.IController;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author kokarez
 */
public class DebugView extends JFrame implements IView {

    IController controller;

    JPanel panel = new JPanel();
    JPanel bottom = new JPanel();
    JButton refresh = new JButton("Refresh");
    
    JScrollPane textPanel = new JScrollPane();
    JTextArea textArea = new JTextArea();

    public DebugView(IController controller) {
        this.controller = controller;
        initUI();
    }

    public final void initUI() {

        setTitle("Debug");
        setSize(640, 800);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        //textPanel.add(textArea);
        panel.add(textArea);
        

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));

        
        bottom.add(Box.createHorizontalGlue());
        bottom.add(refresh);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));

        panel.add(Box.createVerticalGlue());
        panel.add(bottom);
        panel.add(Box.createRigidArea(new Dimension(0, 10)));

        add(panel);
    }

    @Override
    public void Show() {
        setVisible(true);
    }

    @Override
    public void Hide() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getObject(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void action(Integer id, Object... objs) {
        if (id == 0)
            textArea.append((String)objs[0]);
    }

    @Override
    public JComponent getMainPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setMainPanel(JComponent panel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
