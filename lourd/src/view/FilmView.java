/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.IController;
import controller.MainController;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import uk.co.caprica.vlcj.component.EmbeddedMediaListPlayerComponent;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.DefaultFullScreenStrategy;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.FullScreenStrategy;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;
import uk.co.caprica.vlcj.runtime.windows.WindowsCanvas;

/**
 *
 * @author kokarez
 */
public class FilmView implements IView {

    IController controller;
    final JPanel panel = new JPanel();
    JPanel bottom = new JPanel();
    JButton filmlist = new JButton("List des films");
    JButton play = new JButton("Play");
    JButton pause = new JButton("Pause");
    JSlider volume = new JSlider();
    JPanel videoPanel = new JPanel();
    JDialog dlg;
    JProgressBar dpb;
    String filename = null;
    
    private EmbeddedMediaPlayerComponent mediaPlayerFactory;

    public FilmView(IController controller) {
        this.controller = controller;
        initUI();
    }

    public final void initUI() {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));

        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(play);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(pause);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(volume);
        bottom.add(Box.createHorizontalGlue());
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(filmlist);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));

        filmlist.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerFactory.getMediaPlayer().stop();
                network.NetworkSend.StopFlux();
                controller.eventPerfomed(0);
            }
        });

        play.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerFactory.getMediaPlayer().play();
            }
        });

        pause.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerFactory.getMediaPlayer().pause();
            }
        });

        volume.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider slider = (JSlider) e.getSource();
                mediaPlayerFactory.getMediaPlayer().setVolume(slider.getValue());
            }
        });

        dlg = new JDialog((JFrame) MainController.getInstance().getView().getObject("get_frame"), "Progress Dialog", true);
        dpb = new JProgressBar(0, 25);
        dlg.add(BorderLayout.CENTER, dpb);
        dlg.add(BorderLayout.NORTH, new JLabel("Progress..."));
        dlg.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        dlg.setSize(300, 75);
        dlg.setLocationRelativeTo((JFrame) MainController.getInstance().getView().getObject("get_frame"));
        new Thread(new Runnable() {

            public void run() {
                dlg.setVisible(true);
            }
        }).start();

        new Thread(new Runnable() {

            public void run() {
                for (int i = 0; i <= 60000; i++) {
                    dpb.setValue(i % 25);
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {

            @Override
            public void run() {

                mediaPlayerFactory = new EmbeddedMediaListPlayerComponent();


                panel.add(mediaPlayerFactory, BorderLayout.CENTER);
                panel.add(Box.createRigidArea(new Dimension(0, 10)));
                panel.add(bottom);
                panel.add(Box.createRigidArea(new Dimension(0, 10)));
                ((JFrame) MainController.getInstance().getView().getObject("get_frame")).setVisible(true);
                if (filename != null)
                    launchFilm(filename);
            }
        }).start();
    }

    /**
     * Play film on the video player
     * @param film_path film path
     */
    public void launchFilm(String film_path) {

        if (mediaPlayerFactory == null)
        {
            filename = film_path;
            return;
        }
        
        dlg.setVisible(false);
        mediaPlayerFactory.getMediaPlayer().playMedia(film_path);
        volume.setValue(mediaPlayerFactory.getMediaPlayer().getVolume());
        System.out.println("film:" + film_path + " volume:" + mediaPlayerFactory.getMediaPlayer().getVolume());
        ((JFrame) MainController.getInstance().getView().getObject("get_frame")).setVisible(true);

    }

    @Override
    public void Show() {
    }

    @Override
    public void Hide() {
    }

    @Override
    public Object getObject(String name) {
        return null;
    }

    @Override
    public JPanel getMainPanel() {
        return this.panel;
    }

    @Override
    public void setMainPanel(JComponent panel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void action(Integer id, Object... objs) {
        if (id == 0) {
            launchFilm((String) objs[0]);
        }

    }
}
