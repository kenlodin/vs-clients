/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.ActionListener;
import javax.swing.JComponent;

/**
 *
 * @author MMO POWA
 */
public class ViewTools {
    
    /**
     * Calcul position of the component to center it
     * @param width
     * @param paneWidth
     * @return Y position needed
     */
    public static Integer getCenter(Integer width, Integer paneWidth) {
        Integer         res = null;
        
        res = paneWidth / 2;
        res -= width / 2;
        return res;
    }
    
    /**
     * Swing don't repaint correctly it component, this function for it.
     * @param component 
     */
    public static void forceRepaint(JComponent component) {
        component.setVisible(false);
        component.setVisible(true);
    }
    
}
