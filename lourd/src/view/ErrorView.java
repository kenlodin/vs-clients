/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 *
 * @author kokarez
 */
public class ErrorView {

    /**
     * Launch an error dialog
     * @param component current jframe
     * @param msg error message
     */
    public static void error(JComponent component, String msg) {
        JOptionPane.showMessageDialog(component,
                                      msg,
                                      "Error",
                                      JOptionPane.ERROR_MESSAGE);
    }

}
