/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.IController;
import controller.LoginController;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.jar.JarEntry;
import javax.swing.*;
import javax.swing.plaf.TextUI;

/**
 *
 * @author derboi_c
 */
public class LoginView extends JFrame implements IView, ActionListener {

    JButton         connexion = new JButton("Login");
    
    JTextField      login = new JTextField("login");
    JTextField      password = new JPasswordField();
    
    JLabel          label = new JLabel("Bienvenue");
    JLayeredPane    panel = new JLayeredPane();
    
    IController  controller;

    @Override
    public void setMainPanel(JComponent panel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void action(Integer id, Object... objs) {
        if (id == LoginController.Info.ENABLE_LOGIN)
            connexion.setEnabled(true);
        if (id == LoginController.Info.DISABLE_LOGIN)
            connexion.setEnabled(false);
        ViewTools.forceRepaint(connexion);
    }

    
    public class Infos {
        public final static int WIDTH = 200;
        public final static int HEIGHT = 180;
    }
    
    public LoginView(IController  controller) {
        this.controller = controller;
        initUI();
    }
    
    public void center(JComponent component) {
        
    }
    
    public final void initUI() {
        Dimension size;
        
        setTitle("login");
        setSize(Infos.WIDTH, Infos.HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        
        getContentPane().add(panel);

        panel.setBorder(BorderFactory.createEmptyBorder(
                10, //top
                30, //left
                10, //bottom
                30) //right
                );
        
        login.setText("test");
        password.setText("xavier est un gros con");
        
        
        size = login.getPreferredSize();
        login.setBounds(ViewTools.getCenter(160, Infos.WIDTH),
                        40,
                        160, size.height + 5);
        
        size = password.getPreferredSize();
        password.setBounds(ViewTools.getCenter(160, Infos.WIDTH),
                           70,
                           160, size.height + 5);
        
        size = connexion.getPreferredSize();
        connexion.setBounds(ViewTools.getCenter(size.width, Infos.WIDTH),
                            Infos.HEIGHT - (3 * size.height),
                            size.width, size.height);
        
        size = label.getPreferredSize();
        label.setBounds(ViewTools.getCenter(size.width, Infos.WIDTH),
                            12,
                            size.width, size.height);
        
        connexion.addActionListener(this);
        connexion.setEnabled(false);
        
        panel.setLayout(null);
        panel.add(label);
        panel.add(login);
        panel.add(password);
        panel.add(connexion);
        
    }
    
    /************************************************/
    /*             IView interface                  */
    /*                                              */
    /************************************************/
    
    @Override
    public void Show() {
        setVisible(true);
    }

    @Override
    public void Hide() {
        setVisible(false);
    }

    @Override
    public Object getObject(String name) {
        if (name.equals("password"))
            return password.getText();
        if (name.equals("login"))
            return login.getText();
        return null;
    }
    
    @Override
    public JComponent getMainPanel() {
        return this.panel;
    }

    /************************************************/
    /*             Boutton Listenner                */
    /*                                              */
    /************************************************/
    
    @Override
    public void actionPerformed(ActionEvent e) {
        controller.eventPerfomed(0,
                                 login.getText(),
                                 password.getText());
    }
    
    
}
