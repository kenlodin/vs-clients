/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author kokarez
 */
public interface IView  {
    /**
     * Draw view
     */
    public void Show();
    
    /**
     * Hide view
     */
    public void Hide();
    
    /**
     * Get object by his string name commande
     * @param name Name of the object wanted
     * @return Object wanted
     */
    public Object getObject(String name);
    
    /**
     * Make an action by her id (cf Info of the view)
     * @param id Action identifiant
     * @param objs Optinal param needed to make the action
     */
    public void action(Integer id, Object ... objs);
    
    /**
     * Method to get the mainpanel of the view
     * @return Main panel
     */
    public JComponent getMainPanel();
    
    /**
     * Method to set the mainpanel on the view
     * @param panel New mainpanel
     */
    public void setMainPanel(JComponent panel);
}
