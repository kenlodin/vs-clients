/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.IController;
import controller.ListFilmController;
import controller.LoginController;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.*;
import model.FilmListModel;
import model.FilmModel;

/**
 *
 * @author MMO POWA
 */
public class ListFilmView implements IView {



    IController     controller;
    
    JPanel          panel = new JPanel();
    JPanel          bottom = new JPanel();

    JScrollPane     scroll;
    
    JButton         lancer = new JButton("Lancer");
    JButton         logout = new JButton("Logout");
    
    JProgressBar    progressBar = new JProgressBar();

    Vector<String> vectorTitre = new Vector<String>();
    Vector<FilmModel> vectorValue = new Vector<FilmModel>();
    FilmListModel filmListModel;
    
    JTable table;

    public ListFilmView(IController controller) {
        this.controller = controller;
        initUI();
    }
        
    public final void initUI() {    
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));

        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(progressBar);
        bottom.add(Box.createHorizontalGlue());
        bottom.add(lancer);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        bottom.add(logout);
        bottom.add(Box.createRigidArea(new Dimension(10, 0)));
        
        lancer.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int id = table.getSelectedRow();
                if (id == -1)
                    ErrorView.error(panel, "You must select an video");
                else
                    controller.eventPerfomed(0, filmListModel.getFilm(id));
            }
        });
        
        logout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LoginController.getInstance().deconnect();
            }
        });


        vectorTitre.add("Name");
        vectorTitre.add("Type");
        vectorTitre.add("Description");

        filmListModel = new FilmListModel(vectorTitre, vectorValue);
        table = new JTable(filmListModel);

        scroll =  new JScrollPane(table);

        table.setVisible(true);
        panel.add(scroll);
        //ScrollPanel make glue ?!?
        //panel.add(Box.createVerticalGlue());
        panel.add(Box.createRigidArea(new Dimension(0, 10)));
        panel.add(bottom);
        panel.add(Box.createRigidArea(new Dimension(0, 10)));
    }
    
    /**
     * Add film on the current listview
     * @param film new film
     */
    public void Add(FilmModel film) {
        filmListModel.addFilm(film);
        ViewTools.forceRepaint(table);
    }
    
    @Override
    public void Show() {
        panel.setVisible(true);
    }

    @Override
    public void Hide() {
        panel.setVisible(false);
    }

    @Override
    public Object getObject(String name) {
        return null;
    }

    @Override
    public JComponent getMainPanel() {
        return this.panel;
    }

    @Override
    public void setMainPanel(JComponent panel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void action(Integer id, Object... objs) {
        if (id == ListFilmController.Infos.ACTION_ADD_FILM)
            Add((FilmModel)objs[0]);
    }

}
