/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import view.DebugView;
import view.IView;

/**
 *
 * @author kokarez
 */
public class DebugController implements IController {

    private static DebugController instance = null;
    
    long time = System.currentTimeMillis() / 1000;
    
    /**
     * Method to get singleton of film controler
     * @return DebugController instance
     */
    public synchronized static DebugController getInstance() {
        if (instance != null)
            return instance;
        instance = new DebugController();
        return instance;
    }

    public DebugController() {
        this.view = new DebugView(this);
    }

    IView view = null;
    Boolean debug = false;

    public void setDebug(Boolean debug) {
        this.debug = debug;
        if (this.debug)
            this.view.Show();
    }
    
    public void println(String line) {
        if (debug)
            this.view.action(0, "[" + (time - (System.currentTimeMillis()/1000)) + "] " +line + System.getProperty("line.separator"));
        System.out.println(line);
    }

    @Override
    public void eventPerfomed(Integer id, Object... objs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getObject(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
