/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.BorderLayout;
import javax.swing.*;
import model.FilmModel;
import network.NetworkSend;
import view.FilmView;
import view.IView;

/**
 *
 * @author kokarez
 */

public class FilmController implements IController {

    static FilmController instance = null;
    static JDialog dlg;
    
    /**
     * Method to get singleton of film controler
     * @return FilmController instance
     */
    public synchronized static FilmController getInstance() {
        if (instance != null)
            return instance;
        instance = new FilmController();
        return instance;
    }
    
    IView view = null;

    public FilmController() {
        this.view = new FilmView(this);
        //this.view.action(0, "N:\\Nouveau\\test.avi");
    }
        
    /**
     * Method to launch video on the player
     * @param film File path to the video file
     */
    public void playFilm(FilmModel film) {
        dlg = new JDialog((JFrame)MainController.getInstance().getView().getObject("get_frame"), "Progress Dialog", true);
        JProgressBar dpb = new JProgressBar(0, 500);
        dlg.add(BorderLayout.CENTER, dpb);
        dlg.add(BorderLayout.NORTH, new JLabel("Progress..."));
        dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dlg.setSize(300, 75);
        dlg.setLocationRelativeTo((JFrame)MainController.getInstance().getView().getObject("get_frame"));
        NetworkSend.GetFlux(film.getId());
        //this.view.action(0, film);
        //this.view.action(0, "/home/kokarez/Téléchargements/Space.Battleship.2011.FRENCH.DVDRip.XVID-AC3.UTT.CD1.avi");
    }
    
    /**
     * Return the current body panel of the view
     * @return Body panel
     */
    public JComponent getPanel() {
        return view.getMainPanel();
    }

    @Override
    public void eventPerfomed(Integer id, Object... objs) {
        if (id == 0)
            MainController.getInstance().switchView(MainController.Infos.FILM_LIST);
        else if (id == 1)
            this.view.action(0, (String)objs[0]);
    }

    @Override
    public Object getObject(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
