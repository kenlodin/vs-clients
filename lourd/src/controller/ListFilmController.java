/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import model.FilmModel;
import network.NetworkSend;
import view.IView;
import view.ListFilmView;

/**
 *
 * @author kokarez
 */
public class ListFilmController implements IController {

    static ListFilmController instance = null;
    
    /**
     * Method to get singleton of film controler
     * @return ListFilmController instance
     */
    public synchronized static ListFilmController getInstance() {
        if (instance != null)
            return instance;
        instance = new ListFilmController();
        return instance;
    }

    public class Infos {
        public final static int ACTION_ADD_FILM = 10;
    }
    
    private ListFilmController() {
        view = new ListFilmView(this);
        NetworkSend.GetFluxList(0, "*");
    }
    
    /**
     * Perform an action on the view to add film in list
     * @param film new film
     */
    public void addFilm(FilmModel film) {
        view.action(Infos.ACTION_ADD_FILM, film);
    }
    
    IView view = null;
    
    /**
     * Return the current body panel of the view
     * @return Body panel
     */
    public JComponent getPanel() {
        return view.getMainPanel();
    }

    @Override
    public void eventPerfomed(Integer id, Object... objs) {
        if (id == 0)
        {
            MainController.getInstance().switchView(MainController.Infos.FILM_DISPLAY);
            FilmController.getInstance().playFilm((FilmModel)objs[0]);
        }
    }

    @Override
    public Object getObject(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
