/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author kokarez
 */
public interface IController {
    
    /**
     * Performe action by her id on the controller (cf Info of the controller)
     * @param id Action id
     * @param objs Needed param to perform action
     */
    public void eventPerfomed(Integer id, Object ... objs);
    
    /**
     * Get object by his string name commande
     * @param name Name of the object wanted
     * @return Object wanted
     */
    public Object getObject(String name);
}
