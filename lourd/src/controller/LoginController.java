/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import network.NetworkSend;
import view.ErrorView;
import view.IView;
import view.LoginView;

/**
 *
 * @author derboi_c
 */
public class LoginController implements IController {

    static LoginController instance = null;
    
    /**
     * Method to get singleton of film controler
     * @return LoginController instance
     */
    public synchronized static LoginController getInstance() {
        if (instance != null)
            return instance;
        instance = new LoginController();
        return instance;
    }
    
    public class Info {
        public final static int CONNEXION_PUSH = 0;
        public final static int ENABLE_LOGIN = 10;
        public final static int DISABLE_LOGIN = 11;
    }

    private LoginController() {
        view = new LoginView(this);
    }
    
    IView   view = null;
    String  token = null;
    
    /**
     * Set view on the controller
     * @param view new view
     */
    public void setView(IView view) {
        this.view = view;
    }

    /**
     * Draw view
     */
    public void launch() {
        view.Show();
    }

    /**
     * Return token
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * Set token and launch main controller
     * @param token token
     */
    public void setToken(String token) {
        this.token = token;
        lauchMainView();
    }
    
    /**
     * Reset the token and launch authentification
     */
    public void deconnect() {
        token = null;
        MainController.getInstance().launch();
    }
    
    /**
     * Return the state of the authentification
     * @return authentification
     */
    public Boolean isConnected() {
        return (token != null);
    }
    
    /**
     * Authentifiate the user on the master tracker
     * @param login user login
     * @param password user password
     */
    public void Connection(String login, String password) {
        NetworkSend.Authentification(login, password);
        // Thread launch to connexion
        
        // Here the code is temp
        // ErrorView.error(this.view.getMainPanel(), "Bad login/password " + login + "/*****");
        // token = "OK";
        // lauchMainView();
    }
    
    /**
     * Hide view and launch main view
     */
    public void lauchMainView() {
        view.Hide();
        MainController.getInstance().launch();
    }

    /************************************************/
    /*             Action Listenner                 */
    /*                                              */
    /************************************************/

    @Override
    public void eventPerfomed(Integer id, Object... objs) {
        if (Info.CONNEXION_PUSH == id)
            Connection((String)objs[0], (String)objs[1]);
        if (Info.ENABLE_LOGIN == id)
            view.action(Info.ENABLE_LOGIN);
        if (Info.DISABLE_LOGIN == id)
            view.action(Info.DISABLE_LOGIN);
    }

    @Override
    public Object getObject(String name) {
        return null;
    }
    
}
