/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.FilmModel;
import view.IView;
import view.MainView;

/**
 *
 * @author MMO POWA
 */
public class MainController implements IController {

    static MainController instance = null;
    public int current_mode = Infos.FILM_LIST;
    
    /**
     * Method to get singleton of film controler
     * @return LoginController instance
     */
    public synchronized static MainController getInstance() {
        if (instance != null)
            return instance;
        instance = new MainController();
        instance.init();
        return instance;
    }
    
    private void init() {
        this.view = new MainView(this);
    }
    
    @Override
    public void eventPerfomed(Integer id, Object... objs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getObject(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public class Infos {
        public final static int FILM_LIST = 0;
        public final static int FILM_DISPLAY = 1;
    }
    
    IView view = null;

    /**
     * Return the current view associated to the controller
     * @return Current view
     */
    public IView getView() {
        return view;
    }
    
    /**
     * Switch the body panel between ListFilmView and FilmView
     * @param mode View needed
     */
    public void switchView(Integer mode) {
        if (mode == Infos.FILM_LIST)
            this.view.setMainPanel(ListFilmController.getInstance().getPanel());
        if (mode == Infos.FILM_DISPLAY)
            this.view.setMainPanel(FilmController.getInstance().getPanel());
        current_mode = mode;
    }
    
    /**
     * Hide the main view and launch the authentification
     */
    public void sessionOff() {
        view.Hide();
        LoginController.getInstance().launch();
    }
    
    /**
     * Draw main view with film list
     */
    public void sessionOn() {
        switchView(Infos.FILM_LIST);
        //switchView(Infos.FILM_DISPLAY);
        view.Show();
    }
    
    /**
     * Launch interface and check if user is authentified
     */
    public void launch() {
        if (LoginController.getInstance().isConnected())
            sessionOn();
        else
            sessionOff();
    }
    
}
